#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2015

"""
Conveyor is a daemon to manage file transfers.
"""

import argparse
import signal

from rucio.daemons.conveyor.coordinator import run, stop

if __name__ == "__main__":

    signal.signal(signal.SIGTERM, stop)

    parser = argparse.ArgumentParser()
    parser.add_argument("--run-once", action="store_true", default=False,
                        help='One iteration only')
    parser.add_argument("--process", action="store", default=0, type=int,
                        help='Concurrency control: current processes number')
    parser.add_argument("--total-processes", action="store", default=1, type=int,
                        help='Concurrency control: total number of processes')
    parser.add_argument("--total-threads", action="store", default=1, type=int,
                        help='Concurrency control: total number of threads per process')
    parser.add_argument('--threshold-per-rse', action="store", default=5000, type=int,
                        help='Maximum transfers per RSE')
    parser.add_argument('--renew-period', action="store", default=1800, type=int,
                        help='Period of seconds')
    args = parser.parse_args()

    try:
        run(once=args.run_once,
            process=args.process,
            total_processes=args.total_processes,
            total_threads=args.total_threads,
            threshold_per_rse=args.threshold_per_rse,
            renew_period=args.renew_period)
    except KeyboardInterrupt:
        stop()
